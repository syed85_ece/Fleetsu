<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Device extends Model
{

    protected $table = 'device';
    protected $fillable = [
        'deviceId', 'deviceLable', 'status', 'created_at', 'updated_at'
    ];

    /*
        get Device
    */
    public static function  getDevice() {
        $device = DB::select("SELECT `deviceId`, `deviceLable`, `status`, `created_at`, `updated_at` FROM `device` WHERE 1 AND status = 'Active' ORDER BY deviceId DESC");
        return $device;       
    }
}
