/**
*  First Module
*
* Description
*/
var app = angular.module('cleanBlog', [
	'ui.router'
	]);

app.config(['$urlRouterProvider', '$stateProvider', function($urlRouterProvider, $stateProvider){
	$urlRouterProvider.otherwise('/');
	$stateProvider
		.state('home', {
	        url: '/',
	        templateUrl: 'home.html',
	        controller: 'IndexController'
	    })
	    .state('contact', {
	        url: '/contact',
	        templateUrl: 'contact.html'
	    })
}]);