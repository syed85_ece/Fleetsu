-- phpMyAdmin SQL Dump
-- version 4.6.5.1deb3+deb.cihar.com~trusty.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 29, 2017 at 03:52 PM
-- Server version: 5.6.33-0ubuntu0.14.04.1
-- PHP Version: 7.0.13-1+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testProject`
--

-- --------------------------------------------------------

--
-- Table structure for table `device`
--

CREATE TABLE `device` (
  `id` int(11) NOT NULL,
  `deviceId` varchar(255) NOT NULL,
  `deviceLable` varchar(255) NOT NULL,
  `status` enum('Active','Deactive','Delete') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `device`
--

INSERT INTO `device` (`id`, `deviceId`, `deviceLable`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Device001', 'Device01', 'Active', '2017-06-28 12:00:10', '2017-06-28 12:00:10'),
(2, 'Device002', 'Device02', 'Active', '2017-06-29 06:49:49', '2017-06-29 06:49:49'),
(3, 'Device003', 'Device03', 'Active', '2017-06-29 06:55:44', '2017-06-29 06:55:44'),
(4, 'Device004', 'Device04', 'Active', '2017-06-29 06:55:55', '2017-06-29 06:55:55'),
(5, 'Device005', 'Device05', 'Active', '2017-06-29 06:56:05', '2017-06-29 06:56:05'),
(6, 'Device006', 'Device06', 'Active', '2017-06-29 06:56:30', '2017-06-29 06:56:30'),
(7, 'Device007', 'Device07', 'Active', '2017-06-29 06:56:40', '2017-06-29 06:56:40'),
(8, 'Device008', 'Device08', 'Active', '2017-06-29 06:56:49', '2017-06-29 06:56:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `device`
--
ALTER TABLE `device`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `device`
--
ALTER TABLE `device`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
