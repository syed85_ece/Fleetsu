var a = angular.module('cleanBlog');
a.controller('IndexController',function($scope,PostFactory){
	$scope.Details = [];
	PostFactory.post().then(
		function(result) {
			$scope.Details = result.data.device_info;
			console.log($scope.Details);
		},
		function(error) {
			console.log(error);
		}
	);
	$scope.dateDiff = function(d) {
		var fDate = new Date();
		var tDate = new Date(d);
		var hours = Math.abs(fDate.getTime() - tDate.getTime()) / 36e5;
		return (hours > 24) ? true : false;
	}
});