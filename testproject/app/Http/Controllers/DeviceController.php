<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Device;
use DB;
class DeviceController extends Controller
{  
    /*
        get Device
    */
    public function getDevice()
    {
        $data = array();
        $device = Device::getDevice();
        if(count($device)>0) {
            $data['status'] = 'success';
            $data['device_info'] = $device;
        } else {
            $data['status'] = 'error';
            $data['device_info'] = '';
            $data['message'] = 'Device not aviable';
        }
        return json_encode($data);
    }
}
